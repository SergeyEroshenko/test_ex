import websockets
from websockets.client import WebSocketClientProtocol
import ssl
import json
import zlib
import numpy as np
from decimal import Decimal


class BaseConnector:

    def __init__(self, uri: str, subscribe_message: dict):
        # инициализируем экземпляр класса, принимаем нужные параметры
        self.uri = uri
        self.msg = json.dumps(subscribe_message)
        self.last_ticker = None
        self.sim_ticker = None


    async def consume(self):
        # открываем соединение
        async with websockets.connect(
            uri=self.uri, 
            ssl=ssl.SSLContext(ssl.PROTOCOL_TLS)
            ) as websocket:
            await self.subscribe(websocket) # ждем выполнения подписки
            await self.consumer_handler(websocket) # слушаем соединение


    async def subscribe(self, websocket: WebSocketClientProtocol):
        # отправляем в соединение сообщение с параметрами подписки
        await websocket.send(self.msg)


    async def consumer_handler(self, websocket: WebSocketClientProtocol):
        # все поступающие сообщения из соединения отправляем в парсер
        async for message in websocket:
            self._parse_message(message)


    @staticmethod
    def _parse_message(message):
        # метод для обработки сообщений из соединения
        print(message)
    

    def _check(self, new_ticker):
        # проверка на то, что объектом получены значения лучших цен
        # и объемов из стакана
        if self.last_ticker is None:
            self.last_ticker = new_ticker.copy()
            self.sim_ticker = self.last_ticker.copy()


    def update_ticker(self, new_ticker):
        # симуляция стакана (тоько лучший бид и аск)
        if new_ticker[0] == self.last_ticker[0]:
            self.sim_ticker[1] += (new_ticker[1] - self.last_ticker[1])

        else:
            self.sim_ticker[0:2] = new_ticker[0:2]

        if new_ticker[2] == self.last_ticker[2]:
            self.sim_ticker[3] += (new_ticker[3] - self.last_ticker[3])

        else:
            self.sim_ticker[2:] = new_ticker[2:]

        self.sim_ticker[self.sim_ticker<0] = 0
        self.last_ticker = new_ticker.copy()
        
        
class OkexConnector(BaseConnector):
    # создаем наследник базового класса
    # для конкретной биржи из ТЗ
    name = 'okex'

    def __init__(self, trader: object):
        # инициализируем экземпляр класса 
        # на входе принимаем объект, которому отдавать
        # полученную информация
        self.uri = 'wss://real.okex.com:8443/ws/v3'
        self.msg = json.dumps({
                "op": "subscribe", 
                "args": ["spot/depth5:USDC-BTC"]
                })
        self.price_tick = Decimal('0.0000001')
        self.qty_tick = Decimal('0.0001')
        self.trader = trader
        self.last_ticker = None
        self.sim_ticker = None


    def _parse_message(self, message):
        # производим парсинг сообщения
        decompress = zlib.decompressobj(-zlib.MAX_WBITS)  # see above
        inflated = decompress.decompress(message)
        inflated += decompress.flush()
        message = json.loads(inflated)
        # проверяем на наличие полезных данных
        if 'data' not in message.keys():
            return
        # получаем значения лучших цен и объемов
        bid = Decimal(message['data'][0]['bids'][0][0])
        bid_qty = Decimal(message['data'][0]['bids'][0][1])
        ask = Decimal(message['data'][0]['asks'][0][0])
        ask_qty = Decimal(message['data'][0]['asks'][0][1])
        # собираем вектор из полученных значений
        new_ticker = np.array(
            [bid, bid_qty, ask, ask_qty],
            dtype=np.dtype(Decimal)
            )
        # проверяем значения тикера. если нет, задаем новое
        self._check(new_ticker)

        # если информация в новом тикере отличается от предыдущей
        # выполняем обновление симуляции тикера и отправляем 
        # торговому блоку весь объект
        if (new_ticker != self.last_ticker).any():
            
            self.update_ticker(new_ticker)
            self.trader.recv(self)  


class BinanceConnector(BaseConnector):
    # создаем наследник базового класса
    # для конкретной биржи из ТЗ
    name = 'binance'

    def __init__(self, trader: object):
        # инициализируем экземпляр класса 
        # на входе принимаем объект, которому отдавать
        # полученную информация
        self.uri = 'wss://stream.binance.com:9443/ws/btcusdc@trade'
        self.msg = json.dumps({
            "method": "SUBSCRIBE",
            "params": ["btcusdc@bookTicker"],
            "id": 1
            })
        self.price_tick = Decimal('0.01')
        self.qty_tick = Decimal('0.000001')
        self.trader = trader
        self.last_ticker = None
        self.sim_ticker = None


    def _parse_message(self, message):
        # производим парсинг сообщения
        message = json.loads(message)
        # проверяем на наличие полезных данных
        if 'u' not in message.keys():
            return
        # получаем значения лучших цен и объемов
        bid = Decimal(message['b'])
        bid_qty = Decimal(message['B'])
        ask = Decimal(message['a'])
        ask_qty = Decimal(message['A'])
        # собираем вектор из полученных значений
        new_ticker = np.array(
            [bid, bid_qty, ask, ask_qty],
            dtype=np.dtype(Decimal)
            )
        # проверяем значения тикера. если нет, задаем новое
        self._check(new_ticker)
        # выполняем обновление симуляции тикера и отправляем 
        # торговому блоку весь объект
        self.update_ticker(new_ticker)
        self.trader.recv(self)