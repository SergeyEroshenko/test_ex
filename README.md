apt install python3.7

alias python="python3.7"

python -m venv .venv

source .venv/bin/activate

pip install --upgrade pip

pip install -r requirements.txt