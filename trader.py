import logging
import numpy as np
from decimal import Decimal


class Trader:

    def __init__(self):
        # инициализируем экземпляр класса, создаем атрибуты
        self.best_prices = dict()
        self.tick_sizes = dict()
        self.side = str()
        self.sizes = dict()
        # задаем параметры логирования
        logging.basicConfig(
            level=logging.INFO, 
            filename='trader.log', 
            filemode='a', 
            format='%(asctime)s;%(message)s'
            )


    def trade(self):
        # функция для исполнения торговых приказов

        # проверяем объемы на соответствие 
        # минимальным требованиям
        if 0 in [*self.sizes.values()]:
            return

        # узнаем направление сделки
        x = self._check_side()

        # забираем рассчитанные объемы из стакана
        # и логируем сделку
        for name in self.sizes.keys():
            size = self.sizes[name]
            price = self.best_prices[name][x]
            
            self.best_prices[name][x+1] -= size
            
            data = np.zeros(4)
            data[x] = price
            data[x+1] = size
            self.logger(name, self.side, data)


    def recv(self, obj: object):
        # функция для приема информации из соединений

        # принимаем лучшие цены и объемы,
        # а также метаинформацию о шаге объема
        self.best_prices[obj.name] = obj.sim_ticker
        self.tick_sizes[obj.name] = obj.qty_tick

        # логируем принятые цены и объемы
        self.logger(obj.name, 'recv', obj.last_ticker)
        self.logger(obj.name, 'sim', obj.sim_ticker)

        # если получена информация от двух бирж
        # выполняем расчет цен по стратегии
        if len(self.best_prices.keys()) == 2:
            self.calc()

    
    def calc(self):
        # функция для рассчета прибыльности сделки
        # и ее параметров
        names = [*self.best_prices.keys()]

        if self.best_prices[names[0]][0] * self.best_prices[names[1]][0] > 1:
            self.side = 'sell'
            self.calc_size(names)
            self.trade()

        if self.best_prices[names[0]][2] * self.best_prices[names[1]][2] < 1:
            self.side = 'buy'  
            self.calc_size(names)
            self.trade() 


    def calc_size(self, names):   
        # функция для расчета объемов сделок
        # и приведения в соответствие с шагом объема
        x = self._check_side()
        price_0 = self.best_prices[names[0]][x]
        size_0 = self.best_prices[names[0]][x+1]
        price_1 = self.best_prices[names[1]][x]
        size_1 = self.best_prices[names[1]][x+1]
        qty_tick_0 = self.tick_sizes[names[0]]
        qty_tick_1 = self.tick_sizes[names[1]]
        
        if price_0 * size_0 > size_1:
            size_0 = np.floor((size_1*price_1)/qty_tick_0)*qty_tick_0
        
        if price_0 * size_0 < size_1:
            size_1 = np.floor((size_0/price_1)/qty_tick_1)*qty_tick_1 

        self.sizes[names[0]] = size_0
        self.sizes[names[1]] = size_1 


    def _check_side(self):
        # функция задает соответствие текстового вида цены
        # в числовое значение смещения по вектору данных
        if self.side == 'sell':
            x = 0
        
        if self.side == 'buy':
            x = 2    

        return x   


    def logger(self, name, act, data):
        # функция логирования
        data = ';'.join([str(x) for x in data])
        logging.info(f'{name};{act};{data}')