import asyncio
from connectors import OkexConnector, BinanceConnector
from trader import Trader


async def main(conn_0, conn_1):

    await asyncio.gather(
        conn_0.consume(),
        conn_1.consume()
    )

if __name__=='__main__':

    trader = Trader()
    conn_0 = OkexConnector(trader)
    conn_1 = BinanceConnector(trader)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(conn_0, conn_1))
    loop.run_forever()