import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_table

import pandas as pd
import plotly.graph_objs as go


def to_usdc(row):
    # функция для расчета денежного потока в USDC
    if (row['name']=='okex') & (row['action']=='sell'):
        return -row['bid_qty']
    
    if (row['name']=='binance') & (row['action']=='buy'):
        return -row['ask'] * row['ask_qty']    

    if (row['name']=='okex') & (row['action']=='buy'):
        return row['ask_qty']
    
    if (row['name']=='binance') & (row['action']=='sell'):
        return row['bid'] * row['bid_qty'] 
    

def to_btc(row):
    # функция для расчета денежного потока в BTC
    if (row['name']=='okex') & (row['action']=='sell'):
        return row['bid'] * row['bid_qty']
    
    if (row['name']=='binance') & (row['action']=='buy'):
        return row['ask_qty']  

    if (row['name']=='okex') & (row['action']=='buy'):
        return -row['ask'] * row['ask_qty']
    
    if (row['name']=='binance') & (row['action']=='sell'):
        return -row['bid_qty']

# парсим логи
header = ['timestamp', 'name', 'action', 'bid', 'bid_qty', 'ask', 'ask_qty']

df = pd.read_csv('trader.log', sep=';', names=header)
df['timestamp'] = df['timestamp'].astype('datetime64')
# выбираем из логов только торговые события
trades = df[df['action'].isin(['sell', 'buy'])].reset_index(drop=True)
# вычисляем суммарное изменение баланса в USDC
usdc = trades.apply(to_usdc, axis=1)
usdc = usdc[::2].reset_index(drop=True) + usdc[1::2].reset_index(drop=True)
usdc[usdc.abs()<1e-02] = 0
# вычисляем суммарное изменение баланса в BTC
btc = trades.apply(to_btc, axis=1)
btc = btc[::2].reset_index(drop=True) + btc[1::2].reset_index(drop=True)
btc[btc.abs()<1e-07] = 0
# вычисляем изменение баланса USDC на бирже binance
binance_usdc = trades[trades['name']=='binance'].apply(
    lambda x: x['bid'] * x['bid_qty'] - x['ask'] * x['ask_qty'], axis=1
)
# вычисляем изменение баланса USDC на бирже okex
okex_usdc = trades[trades['name']=='okex'].apply(
    lambda x: x['ask_qty'] - x['bid_qty'], axis=1
)
# вычисляем изменение баланса BTC на бирже binance
binance_btc = trades[trades['name']=='binance'].apply(
    lambda x: x['ask_qty'] - x['bid_qty'], axis=1
)
# вычисляем изменение баланса BTC на бирже okex
okex_btc = trades[trades['name']=='okex'].apply(
    lambda x: x['bid'] * x['bid_qty'] - x['ask'] * x['ask_qty'], axis=1
)
# создаем словарь для постраения графиков
graph_data = {
    'usdc': usdc,
    'btc': btc
}
# создаем датафрейм для таблицы изменений балансов по каждой бирже и валюте
table_data = pd.DataFrame(
    data=[
        ['binance', 'USDC', 10000, 10000 + binance_usdc.sum(), binance_usdc.sum()],
        ['binance', 'BTC', 1, 1 + binance_btc.sum(), binance_btc.sum()],
        ['okex', 'USDC', 10000, 10000 + okex_usdc.sum(), okex_usdc.sum()],
        ['okex', 'BTC', 1, 1 + okex_btc.sum(), okex_btc.sum()]
    ], 
    columns=['Биржа', 'Валюта', 'Было', 'Стало', 'Проторговано']
).round(6)
# создаем датафрейм для таблицы суммарного изменения балансов по каждой валюте
sum_table_data = pd.DataFrame(
    data=[
        ['USDC', usdc.sum().round(2)], 
        ['BTC', btc.sum().round(7)]
    ],
    columns=['Валюта', 'Общий результат']
)

# ссылка на css стиль для webgui
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
# создаем приложение для webgui
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = 'Торговый отчет'
# создаем слой с интерактивным графиком, выпадающим меню,
# и двумя таблицами
app.layout = html.Div([
    dcc.Graph(id='graph'),
    dcc.Dropdown(
        id='dropdown',
        options=[
            {'label': 'USDC', 'value': 'usdc'},
            {'label': 'BTC', 'value': 'btc'},
        ],
        style={
            'width': '280px',
            'padding-left': '80px'
            },
        value='usdc'
    ),
    dash_table.DataTable(
        id='table',
        columns=[{'name': i, 'id': i} for i in table_data.columns],
        data=table_data.to_dict('records'),
        style_table={
            'width': '520px',
            'padding-left': '80px',
            'padding-top': '40px'
        },
        style_cell={
            'fontSize': 14,
            'font-family': 'sans-serif'
        },
        style_cell_conditional=[
            {'if': {'column_id': 'Биржа'},
            'width': '80px'},
            {'if': {'column_id': 'Валюта'},
            'width': '80px'},
            {'if': {'column_id': 'Было'},
            'width': '120px'},
            {'if': {'column_id': 'Стало'},
            'width': '120px'}
        ]
    ),
    dash_table.DataTable(
        id='sum_table',
        columns=[{"name": i, "id": i} for i in sum_table_data.columns],
        data=sum_table_data.to_dict('records'),
        style_table={
            'width': '280px',
            'padding-left': '80px',
            'padding-top': '40px'
        },
        style_cell={
            'fontSize': 14,
            'font-family': 'sans-serif'
        },
        style_cell_conditional=[
            {'if': {'column_id': 'Валюта'},
            'width': '80px'},
            {'if': {'column_id': 'Общий результат'},
            'width': '200px'}
        ]
    )
], style={'width': '800px', 'align': 'center'})

# создаем функцию для построения графиков в зависимости
# от состояния выпадающего списка. 
# для связи элементов, оборачиваем функцию в декоратор
# с возможностью обратного вызова функций
@app.callback(Output('graph', 'figure'), [Input('dropdown', 'value')])
def update_graph(name):

    df = graph_data[name]
    fig = go.Figure(
        data=[go.Scatter(x=df.index, y=df.cumsum().values, mode='lines')],
        layout={
            'width': 800,
            'title': f'Результаты торгов в {name.upper()}',
            'xaxis_title': 'Сделки',
            'yaxis_title': name.upper()
            }
        )
    fig.update_layout(title_x=0.5)

    return fig

# запускаем приложение на сервере
app.run_server(debug=True, host='0.0.0.0')